// console.log("elo word");

// function printInfo(){
// 	let nickname = prompt("Ener your nicknaem: ");
// 	console.log("Hi, ${nickname}");
// 	console.log("Hi, " + nickname);
// }

// printInfo();

function printName(name){
	console.log("My name is "+ name);
}

printName("Juana"); // argument = Juana


// Parameter
	// name is called a parameter
	// a parameter acts as a named variable/container that exists only inside a function


let sampleVariable = "Inday";

printName(sampleVariable);

// Variables can also be passed as an argument

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of "+num+" divided by 8 is: "+ remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is "+num+" divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);

// Function as argument

		// Function aprameters can also accept functions as arguments
		// some complex cuntions uses other functions to perform more

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed.");
		}

		function invokeFunction(argumentFunction){
			argumentFunction();
		}

		invokeFunction(argumentFunction);
		console.log(argumentFunction);


	// Using Multiple Parameters

		function createFullName(firstName, middleName, lastName){
			console.log("My full name is "+ firstName + " " + middleName + " " + lastName);
		}

		createFullName("Christopher", "Katigbak", "Malinao");

		let firstName = "John";
		let middleName = "Doe";
		let lastName = "It";

		createFullName(firstName, middleName, lastName);
//----------------------------------------------------------------------


		function getDifferenceOf8Minus4(numA, numB){
			console.log("Difference: "+ (numA - numB));
		}

		getDifferenceOf8Minus4(8,4);


// Return Statement = the return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called

	function returnFullName(firstName, middleName, lastName){
		// return firstName + " " + middleName + " " + lastName;

		// 

		// we could also create a variable inside the function to contain the result and return the variable instead.
		let fullName = firstName + " " + middleName + " " + lastName;
		return fullName;

		console.log("This is printed inside a function");
	}

	let completeName = returnFullName("Jeffrey", "Smith", "Jordan");

	console.log(completeName);

	console.log("I am "+completeName);

function printPlayerInfo(userName, level, job){
	console.log("Username: " + userName);
	console.log("Level: "+ level);
	console.log("Job: "+ job);
}

let user1 = printPlayerInfo("boxzMapagpahal", "Senior", "Programmer");

console.log(user1); // returns undefined